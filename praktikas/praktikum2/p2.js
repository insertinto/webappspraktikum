//------1------
//a)
let hermann = {
    'vorname':'Hermann',
    'alter':44,
    'printName':function(){
        console.log("Vorname: " + this.vorname);
        console.log("Alter: " + this.alter);
    }
};

//b)
let ilse = {
    'vorname':'Ilse',
    'alter':44,
    'printName':function(){
        console.log('Vorname: ' + this.vorname);
        console.log('Alter: ' + this.alter);
    }
};

//c)
let schmitz = {
    'name':'Schmitz',
    'vater':hermann,
    'mutter':ilse,
    'printFamily':function(){
        console.log("_Eltern von" + this.name);
        console.log("_Vater_");
        console.log("vorname: " + this.vater.vorname);
        console.log("alter: " + this.vater.alter);
        console.log("_Mutter_");
        console.log("vorname: " + this.mutter.vorname);
        console.log("alter: " + this.mutter.alter);
    }
};

schmitz.printFamily();

//------2------
//a)

function Person(vorname, alter){
    this.vorname = vorname;
    this.alter = alter;
}

//a
let hermann2 = new Person('hermann',44);
let ilse2 = new Person('ilse',44);

//b
console.log("_____After Protoype______");
/*Vorteil vom Prototyping:
* Durch das Prototyping können wir allen erzeugten Objekten die gleichen Properties mitgeben. Zudem können
* Properties durch das Prototype-Chaining an weitere Objekte vererbt werden.
*
* */
Person.prototype.printName = function(){
    console.log('Vorname: ' + this.vorname);
    console.log('Alter: ' + this.alter);
};
hermann2.printName();
ilse2.printName();

//c)
function Schmitz(vater,mutter){
    this.name = 'Schmitz';
    this.vater = vater;
    this.mutter = mutter;
    this.printFamily = function(){
        console.log("_Eltern von" + this.name);
        console.log("_Vater_");
        console.log("vorname: " + this.vater.vorname);
        console.log("alter: " + this.vater.alter);
        console.log("_Mutter_");
        console.log("vorname: " + this.mutter.vorname);
        console.log("alter: " + this.mutter.alter);
    }
}

let schmitz2 = new Schmitz(hermann2,ilse2);

console.log("schmitz2 family:");
console.log(schmitz2.printFamily());

//d
//AccessorProperty definieren um familyName zu setzen und auszulesen
let schmitz3 = new Schmitz(hermann2,ilse2);

// Object.defineProperty(schmitz3,'familyName',{
Object.defineProperty(Object.getPrototypeOf(schmitz3),'familyName',{
    get:function(){
        return this.name;},
    set:function(name){
        this.name = name}
    ,configurable:true //Benötigte zusatzeigenschaft um die Property löschen zu können
});

console.log("schmitz3 familyname read: " + schmitz3.familyName);
console.log("set schmitz 3 familyname to mayer");
schmitz3.familyName = 'mayer';
console.log("schmitz3 familyname read: " + schmitz3.familyName);

//e
//Accessor Property entfernen.
/*Diese kann nicht entfernt werden, da die Property-Eigenschaft "Configurable" auf False ist. Erst wenn dieser wert
* bei der Definition der Property auf True gesetzt wird, kann die Property gelöscht werden.*/

delete schmitz3.familyName; //=> kein Effekt ohne das Configurable-Attribut auf true
console.log("schmitz3 familyname read: " + schmitz3.familyName);
delete schmitz3.name; //=> IV wird gelöscht.
console.log("schmitz3 familyname read: " + schmitz3.familyName);

//f
//Alle Properties eines Objektes anzeigen lassen
let keys = Object.keys(schmitz3);
console.log(keys);

let dtest = Object.getOwnPropertyDescriptors(schmitz3);
console.log(dtest);

//g
//Siehe instanz von "schmitz" in aufgabe 1c

//Object.freeze(schmitz)
Object.defineProperty(schmitz,'name',{
   writable:false
});

schmitz.name='mayer';
console.log(schmitz.name);

schmitz.mutter='hermann2';
schmitz.vater='ilse2';
console.log(schmitz.mutter);
console.log(schmitz.vater);
//Namen der Eltern können nach wie vor geändert werden, da dessen Writable eigenschaft true ist.

//------3------
//a)

/*TODO: FRAGE, wieso gibt es eine recursion-exception, wenn ich new Object das "hermann" mit übergebe?*/

let proto={};
Object.defineProperty(proto,'vorname',{
    get:function(){
        return this.vorname;},
    set:function(name){
        this.vorname = name},
    // writable:true
    //Uncaught TypeError: property descriptors must not specify a value or be writable when a getter or setter has been specified
    //Das passiert weil laut definition beim setzen von getter&setter das writable attribut nicht gesetzt werden muss.
});

// let hermann3 = new Object(literals);
let hermann3 = Object.create(hermann,proto);
console.log(""+hermann3.vorname);


console.log("______3______");
hermann3.vorname='hermann123';
console.log(hermann3.vorname);
/*Wenn writable nicht auf true gesetzt wird und wir versuchen auf die vorname properties zuzugreifen, erscheint eine
* recursion exception.*/

//------4------
//a
class Hermann4{
    constructor(vorname,alter){
        this.vorname = vorname;
        this.alter = alter;
    }

    printName(){
        console.log("Vorname: " + this.vorname);
        console.log("Alter: " + this.alter);
    }
}

generatedNumbers = new Hermann4("hermann",44);
test.printName();




