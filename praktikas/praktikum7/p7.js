//Aufgabe 1
//a)
/*  −pending: Wartend, Funktion noch nicht abgeschlossen
    −fulfilled: Funktion wurde erfolgreich abgeschlossen
    −rejected: Funktion wurde mit Fehler abgebrochen */
//Platzhalterobject für das zu erwartende Ergebnis
let waitAndReturn = function(ms){
    return new Promise(
        function (resolve,reject){
        setTimeout(()=>{
            resolve(ms);
            reject("Fehler");
        },ms);
    });
};
waitAndReturn(2000).then((ret)=>{console.log("1. Lauf: "+ret)});

//b
waitAndReturn(2000).then((ret)=>{
    console.log("1. Lauf: "+ret);
    waitAndReturn(2000).then((ret)=>{console.log("2. Lauf: "+ret)});
});

//c
console.log("___1c___");
waitAndReturn(1000).then((ret)=>{console.log("1. Lauf: "+ret)});
waitAndReturn(500).then((ret)=>{console.log("2. Lauf: "+ret)});

//d
let asyncWaitAndReturn = async function() {
    try{
        let run1 = await waitAndReturn(4000);
        let run2 = waitAndReturn(2000);
        console.log("1.Lauf: "+ run1);
        //await notwending da sonst noch im pending-state
        console.log("2.Lauf: "+ await run2);
    }catch(e){
        console.log(e);
    }
};
asyncWaitAndReturn();

//e
let asyncWaitAndReturn2 = async function() {
    try{
        let run1 = waitAndReturn(4000);
        let run2 = waitAndReturn(2000);
        /*Beide awaits notwendig, da sonst eine Funktion im Pending-state bleibt
        Ausgabe findet jedoch zeitgleich statt und nicht zeitversetzt. Wieso???*/
        console.log("1.Lauf: "+ await run1);
        console.log("2.Lauf: "+ await run2);
    }catch(e){
        console.log(e);
    }
};
asyncWaitAndReturn2();

//Aufgabe 2
let article1 = new Article("Erdbeeren");

let LimitedAccessProxy = function(maxAccess) {
    let _accessCounter = 0;

    this.get = function(object,property){
        if (_accessCounter < maxAccess){
            _accessCounter++;
            return object[property];
        }else{
            return undefined;
        }
    }
};

limitAccessProxy1 = new LimitedAccessProxy(3);
let proxy = new Proxy(article1,limitAccessProxy1);

console.log(proxy.name);
console.log(proxy.name);
console.log(proxy.name);
console.log(proxy.name);
console.log(proxy.name);
console.log(proxy.name);
