let Article = function(name){

    let _id = Date.now().toString();
    let _name = name == null ? _id + "__name" : name;

    Object.defineProperty(this,"id",{
        get: function () {
            return _id;
        }
    });

    Object.defineProperty(this,"name",{
        get: function () {
            return _name;
        },
        set: function (value) {
            if(!(value.toString().length === 0)){
                _name = value;
            }
        }
    });


};
Article.prototype.printArticle = function(){
    console.log("Article_Name: " + this.name);
    console.log("Article_Id: " + this.id);
};


