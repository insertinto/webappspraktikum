#include <stdio.h>
#include <emscripten.h>

int cFaculty(int number){
    int fact = 1;
    if (number < 0)
        printf("Error! Factorial of a negative number doesn't exist. \n");
    else {
        for (int i = 1; i <= number; ++i) {
            fact *= i;
        }
    }
    return fact;
}

/*
int main(){
    int a = cFaculty(5);
    printf("%d \n", a);
}*/
