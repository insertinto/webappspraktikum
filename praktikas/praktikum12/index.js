const facResultJS = document.getElementById("facResultJS");
const facResultC = document.getElementById("facResultC");

let faculty = function(number){
    let rval=1;
    for (let i = 2; i <= number; i++){
        rval = rval * i;
    }
    return rval;
};

let writeValue= function(field, value , type){
    field.innerText = "Die Fakultät der "+ type + "-Funktion ist: " + value;
};

let calulateJsFaculty = function(){
    let f = faculty(document.getElementById("fnumber").value);
    writeValue(facResultJS,f,"JS")
};

let calulateCFaculty = function(){
    let js_wrapped_add = Module.cwrap("cFaculty","number",["number"]);
    let res = js_wrapped_add(document.getElementById("fnumber").value);
    writeValue(facResultC,res,"C");
};
