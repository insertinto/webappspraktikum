var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define("Comparable", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
});
define("Article", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Article = void 0;
    var Article = /** @class */ (function () {
        function Article(name, description) {
            this.name = name;
            this.description = description;
        }
        Article.prototype.printArticle = function () {
            console.log('name: ' + this.name + ' beschreibung : ' + this.description);
        };
        Article.prototype.compareable = function () {
            console.log("compareable implementiert");
        };
        return Article;
    }());
    exports.Article = Article;
});
define("app", ["require", "exports", "Article"], function (require, exports, Article_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var article_1 = new Article_1.Article("test", "testdes");
    //1b - Typfehler
    // let article_1 = new Article(1,"testdes");
    article_1.printArticle();
    console.log("hello");
});
define("Management", ["require", "exports", "Article"], function (require, exports, Article_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Management = void 0;
    var Management = /** @class */ (function () {
        function Management(articles) {
            this.addArticles = function (amount) {
                for (var i = 0; i < amount; i++) {
                    var article = new Article_2.Article("Article #" + (this.articles.length + 1), "random description");
                    article.price = Math.floor(Math.random() * 100);
                    this.articles.push(article);
                }
            };
            this.removeArticles = function (amount) {
                for (var i = 0; i < amount; i++) {
                    this.articles.pop();
                    if (this.articles.length === 0) {
                        break;
                    }
                }
            };
            this.articles = articles;
        }
        return Management;
    }());
    exports.Management = Management;
});
define("Shoe", ["require", "exports", "Article"], function (require, exports, Article_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.shoe = void 0;
    var shoe = /** @class */ (function (_super) {
        __extends(shoe, _super);
        function shoe(name, description, size) {
            var _this = _super.call(this, name, description) || this;
            _this.size = size;
            return _this;
        }
        shoe.prototype.printShoe = function () {
            _super.prototype.printArticle.call(this);
            console.log("Size:" + this.size);
        };
        return shoe;
    }(Article_3.Article));
    exports.shoe = shoe;
});
