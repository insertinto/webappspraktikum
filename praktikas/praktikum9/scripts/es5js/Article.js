"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Article = /*#__PURE__*/function () {
  function Article(name, price) {
    _classCallCheck(this, Article);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "price", void 0);

    this.name = name; // this.price = price;
  }

  _createClass(Article, [{
    key: "printArticle",
    value: function printArticle() {
      // console.log(name + "name" + price + "price")
      console.log(name + "name");
    }
  }]);

  return Article;
}();