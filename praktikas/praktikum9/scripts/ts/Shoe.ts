//3
import {Article} from "./Article";

export class shoe extends Article{
    private size:number;

    constructor(name:string,description:string,size:number){
        super(name,description);
        this.size = size;
    }

    printShoe(){
        super.printArticle();
        console.log("Size:" + this.size);
    }
}