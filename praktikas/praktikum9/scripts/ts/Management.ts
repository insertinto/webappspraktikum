import {Article} from './Article';

export class Management{
    private articles:any;

    constructor(articles:any){
        this.articles = articles;
    }

    addArticles = function (amount:number) {
        for (let i = 0; i < amount; i++){
            let article:any = new Article("Article #" + (this.articles.length + 1) , "random description");
            article.price = Math.floor(Math.random()*100);
            this.articles.push(article);
        }
    };
    removeArticles = function (amount:number) {
        for (let i = 0; i < amount; i++){
            this.articles.pop();
            if(this.articles.length === 0){break;}
        }
    };
}