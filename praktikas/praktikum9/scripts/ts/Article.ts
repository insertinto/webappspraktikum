import {Comparable} from './Comparable';

export class Article implements Comparable{
    private readonly name:string;
    private readonly description:string;

    constructor(name:string ,description:string){
        this.name = name;
        this.description = description;
    }

    printArticle(){
        console.log('name: ' + this.name + ' beschreibung : ' + this.description);
    }

    compareable(){
        console.log("compareable implementiert");
    }
}


