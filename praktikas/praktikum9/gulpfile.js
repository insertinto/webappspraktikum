const gulp = require('gulp');
// var series = require('gulp');

// const concat = require('gulp-concat');
// const rename = require('gulp-rename');
const ts = require('gulp-typescript');
const babel = require('gulp-babel');

const tsFiles = 'scripts/ts/*.ts';
const tsDest = 'scripts/js';

const es6files = 'scripts/es6js/*.js';
const es5files = 'scripts/es5js';

let typeScript = function() {
    return gulp.src(tsFiles)
        .pipe(ts({
            noImplicitAny : true,
            out : 'out.js',
            module : 'amd',
            target: 'ES5'
        }))
        .pipe(gulp.dest(tsDest));
};

gulp.task('ts', typeScript);

let doBabel = function () {
    return gulp.src(es6files)
        .pipe(babel({
            presets:['@babel/preset-env']
        }))
        .pipe(gulp.dest(es5files));
};

gulp.task('babel', doBabel);

