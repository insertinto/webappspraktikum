//1
let getHandler = function(){
    let body = document.getElementsByTagName("body")[0];
    let state = true;
    let setRed = function(){
        body.setAttribute("style","background-color: red");
        state = true;
    };
    let setBlue = function(){
        body.setAttribute("style","background-color: blue");
        state = false;
    };
    let handle = function(){
        if(!state)
            setRed();
        else
            setBlue();
    };
    return handle;
};

let togglerButton = document.getElementById("switchColor");
togglerButton.addEventListener("click",getHandler());


//-----2-----
//a
let studenten = [
    {name:'Student_b' , alter:22 , geschlecht:'m' , semester:2},
    {name:'Student_c' , alter:23 , geschlecht:'m' , semester:3},
    {name:'Student_a' , alter:21 , geschlecht:'m' , semester:1},
    {name:'Student_d' , alter:24 , geschlecht:'w' , semester:4},
    {name:'Student_e' , alter:25 , geschlecht:'w' , semester:5},
    {name:'Student_f' , alter:26 , geschlecht:'w' , semester:6},
    {name:'Student_j' , alter:30 , geschlecht:'m' , semester:10},
    {name:'Student_j1' , alter:30 , geschlecht:'m' , semester:10},
    {name:'Student_j2' , alter:30 , geschlecht:'m' , semester:10},
    {name:'Student_g' , alter:27 , geschlecht:'w' , semester:7},
    {name:'Student_h' , alter:28 , geschlecht:'m' , semester:8},
    {name:'Student_i' , alter:29 , geschlecht:'w' , semester:9}
];

//b
function getMaxSemester(){
    return studenten.reduce(function(a,b){
        return a.semester > b.semester ? a : b;
    }).semester;
}
console.log("getMaxSemester: " + getMaxSemester());

//c
function getMinSemester(){
    return studenten.reduce(function(a,b){
        return a.semester < b.semester ? a : b;
    }).semester;
}
console.log("getMinSemester: " + getMinSemester());

//d
let semesterFilter = function(semester){
    return function(student){
        return student.semester === semester;
    };
};
console.log("Semester Filter 3");
console.log(studenten.filter(semesterFilter(3)));

//e
console.log("2 e, Kombination der Funktionen");
let studentsInHighestSemester = function (){
    return function (student){
        return student.semester === getMaxSemester();
    };
};

//f
let geschlechtsFilter = function(geschlecht){
    return function(student){
        return student.geschlecht === geschlecht;
    };
};
console.log("GeschlechtsFilter M");
console.log(studenten.filter(geschlechtsFilter("m")));

//g
// Salutation
function generateSalutation() {
    return function (student) {
        return {
            anrede: student.geschlecht === 'm' ? "herr" : "frau",
            name: student.name,
            semester: student.semester + ".Semester"
        };
    }
}

//h
function printStudent(student){
    console.log(student.anrede + " " + student.name+ " "+ student.semester)
}
let modifiedStudents = generateSalutation();

//i
console.log("2 i)");
studenten.filter(studentsInHighestSemester()).map(generateSalutation()).forEach(printStudent);

//j
console.log("2 j)");
studenten.filter(semesterFilter(getMinSemester())).map(generateSalutation()).forEach(printStudent);

// k
console.log("2 k)");
console.log(studenten.filter(geschlechtsFilter("w")).map(modifiedStudents).forEach(printStudent));

// l
console.log("2 l)");
console.log(studenten.filter(geschlechtsFilter("m")).filter(semesterFilter(3)).map(modifiedStudents).forEach(printStudent));

//Aufgabe 3
//a
let quadFunk = function(a){
    return function(b){
        return function(c){
            return function(x){
                return a*(x*x) + b*x + c;
            }
        }
    }
};

let value = quadFunk(2)(3)(-5);
console.log("Quadratische Funktion");
console.log(value(-2)); //-3
console.log(value(0)); //-5
console.log(value(2)); //9

//Aufgabe 4
let quadFunkSelf = function(a, b, c){
    quadFunkSelf = function (x) {
        return (a*(x*x) + b*x + c);
    };
};

quadFunkSelf(2,3,-5);

console.log("Quadratische Funktion self defining");
console.log(quadFunkSelf(-2)); //-3
console.log(quadFunkSelf(0)); //-5
console.log(quadFunkSelf(2)); //9
