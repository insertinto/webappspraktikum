//Aufgabe 2;
//a)
console.log("2 a)");
let article_1 = new Article();
article_1.printArticle();

//b
console.log("2 b)");
article_1.name = "Blaubeeren";
article_1.printArticle();

//c
console.log("2 c)");
let article_2 = new Article("Erdbeeren");
article_2.description = "Leckere Erdbeeren frisch vom Feld";
article_2.price = 5;

console.log(article_2.isCheaperThan(article_1)); // exception da kein preis für artikel 1 gesetzt
article_1.price = 4;

console.log(article_2.isCheaperThan(article_1)); // true da preis gesetzt

//d
console.log("2 c)");
article_2.specialPrice = 3;
article_1.printArticle();
article_2.printArticle();
console.log("Artikel 2 günstiger als Artikel 1: " + article_2.isCheaperThan(article_1)); //true wegen sonderpreis

//e
console.log("2 e)");
article_2.removeSpecialPrice();
console.log("Artikel 2 günstiger als Artikel 1: " + article_2.isCheaperThan(article_1)); //false weil sonderpreis entfernt.

//4
//a
let shoe_1 = new Shoe(43);
shoe_1.name = "Nike";
shoe_1.description = "white";
shoe_1.price = 60;
shoe_1.printArticle();

let shoe_2 = new Shoe(40);
shoe_2.name = "Adidas";
shoe_2.description = "blue";
shoe_2.price = 50;
shoe_2.printArticle();

//b
console.log("Schuh 2 günstiger als Schuh 1: " + shoe_2.isCheaperThan(shoe_1)); //false weil sonderpreis entfernt.

//c
console.log("--------------------------------");
let shoe_3 = new Shoe(3);
shoe_3.name = "Nike";
shoe_3.price = 60;
shoe_3.printArticle();
shoe_3.price = 50;
shoe_3.printArticle();
shoe_3.specialPrice = 3;
shoe_3.printArticle();

shoe_3.removeSpecialPrice();
shoe_3.printArticle();


