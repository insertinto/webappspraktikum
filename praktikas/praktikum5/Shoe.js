//3
let Shoe = function(size){
    Article.call(this);
    let _size = size;
    let _brand;
    let _price;
    let _specialPrice = null;

    Object.defineProperty(this,"size",{
        get: function () {
            return _size;
        }
    });
    Object.defineProperty(this,"brand",{
        get: function () {
           return _brand;
        },
        set: function (value) {
            _brand = value;
        }
    });

    //---------- START
    /* Hier absolute Redundanzen. Wie kann ich mit Vorbedingung lesend und
       schreibend auf die Parent-Properties zugreifen? */
    Object.defineProperty(this,"price",{
        get: function () {
            if(_specialPrice){
                return _specialPrice;
            }else{
                return _price;
            }
        },
        set: function (value) {
            if(typeof value === "number" && value >= 5){
                _price = value;
            }
        },
    });
    Object.defineProperty(this,"specialPrice",{
        set: function (value) {
            if(typeof value === "number" && value >= 0){
                _specialPrice = value;
            }
        },
    });
    this.removeSpecialPrice = function(){
        _specialPrice = null;
    };
    //---------- Ende


};

Shoe.prototype = new Article();
delete Article.prototype.price;

//TODO:
//überflüssige properties entfernen