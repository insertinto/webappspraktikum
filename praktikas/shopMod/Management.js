let Management = function (articles) {
    let _articles = articles;
    this.addArticles = function (amount) {
        for (let i = 0; i < amount; i++){
            let article = new Article("Article #" + (_articles.length + 1));
            article.price = Math.floor(Math.random()*100);
            _articles.push(article);
        }
    };

    this.removeArticles = function (amount) {
        for (let i = 0; i < amount; i++){
            _articles.pop();
            if(_articles.length === 0){break;}
        }
    };

    this.listArticles = function () {
        return _articles;
    }
};