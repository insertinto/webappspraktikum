//---1---
//a
let article_1 = new Article("Blaubeeren");
article_1.description = "Leckere Blaubeeren frisch vom Feld";
article_1.price = 10;

let article_2 = new Shop.Article("Erdbeeren");
article_2.description = "Leckere Erdbeeren frisch vom Feld";
article_2.price = 5;

console.log(article_2.isCheaperThan(article_1)); // True

if(Interface.implements(article_1,Compareable)){
    console.log("Interface Compareable ist implementiert");
    console.log(article_1.compare(article_2, article_1)); // True
}else{
    throw new Error("Aritkel hat das Compareable interface nicht implementiert");
}

//b
let article_3 = new Shop.Article("Bananen");
article_3.printArticle();

//---2---
//a
let participants = [
    {name:'Student_A' , age:20 },
    {name:'Student_B' , age:23 },
    {name:'Student_C' , age:24 },
    {name:'Student_D' , age:21 },
    {name:'Student_E' , age:26 },
    {name:'Student_F' , age:27 },
    {name:'Student_G' , age:25 },
    {name:'Student_H' , age:29 },
    {name:'Student_I' , age:32 },
    {name:'Student_J' , age:32 }
];

//b
let determineWinner = function(people){
    //Kann nicht direkt returnt werden.
    let [place1, , , place2] = people;
    return [place1,place2]
};
console.log("Gewinner: " + determineWinner(participants));

//c
let printParticipants = function(person){
    let{name, age} = person;
    console.log("Name: "+name);
    console.log("Alter: "+age);
};
printParticipants(determineWinner(participants)[0]);
printParticipants(determineWinner(participants)[1]);

//---3---
//a
console.log("__3a: ");
participants[Symbol.iterator] = function(){
    let index = 0;
    let that = this;
    return {
        next: function(){
            return index < that.length ?
            {value: {   name: that[index].name,
                        age : that[index++].age -1}, done:false} : {done:true};
        }
    }
};
console.log("3a:");
for (let winner of determineWinner(participants))
    printParticipants(winner);


//---4---
let randomGenerator = function*(n,max){
    let min = 1;
    n = !n ? 6 : n;
    max = !max ? 49 : max;

    for(let i = 0 ; i < n ; i++){
        yield Math.floor(Math.random()*(max-min+1)+min);
    }
};

let generatedNumbers = randomGenerator(5, 6);
for(let number of generatedNumbers){
    console.log(number);
}
