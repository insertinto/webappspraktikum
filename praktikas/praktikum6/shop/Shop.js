let Shop = (function(){
    let Article = function(name){

        let _id = Date.now().toString();
        let _description = null;
        let _price = null;
        let _specialPrice = null;
        let _name = name == null ? _id + "__name" : name;

        Object.defineProperty(this,"id",{
            get: function () {
                return _id;
            }
        });

        Object.defineProperty(this,"name",{
            get: function () {
                return _name;
            },
            set: function (value) {
                if(!(value.toString().length === 0)){
                    _name = value;
                }
            }
        });

        Object.defineProperty(this,"description",{
            get: function () {
                return _description;
            },
            set: function (value) {
                _description = value;
            }

        });

        Object.defineProperty(this,"price",{
            get: function () {
                if(_specialPrice){
                    return _specialPrice;
                }else{
                    return _price;
                }
            },
            set: function (value) {
                if(typeof value === "number" && value >= 0){
                    _price = value;
                }
            },
            configurable: true
        });

        Object.defineProperty(this,"specialPrice",{
            set: function (value) {
                if(typeof value === "number" && value >= 0){
                    _specialPrice = value;
                }
            },
            configurable: true
        });

        this.removeSpecialPrice = function(){
            _specialPrice = null;
        };

        let compare = function(article1 , article2){
            return article1.isCheaperThan(article2);
        };

    };

    Article.prototype.printArticle = function(){
        console.log("__Article: " + this.name);
        console.log("_Id: " + this.id);
        console.log("_Description: " + this.description);
        console.log("_Price: " + this.price);
        // console.log("_SpecialPrice: " + _specialPrice);
    };

    Article.prototype.isCheaperThan = function(article){
        if(!this.price || !article.price){
            console.log( "Both articles must have price for comparison.");
        }else{
            return this.price < article.price;
        }
    };


    let Shoe = function(size){
        Article.call(this);
        let _size = size;
        let _brand;
        let _price;
        let _specialPrice = null;

        Object.defineProperty(this,"size",{
            get: function () {
                return _size;
            }
        });
        Object.defineProperty(this,"brand",{
            get: function () {
                return _brand;
            },
            set: function (value) {
                _brand = value;
            }
        });

        //---------- START
        /* Hier absolute Redundanzen. Wie kann ich mit Vorbedingung lesend und
           schreibend auf die Parent-Properties zugreifen? */
        Object.defineProperty(this,"price",{
            get: function () {
                if(_specialPrice){
                    return _specialPrice;
                }else{
                    return _price;
                }
            },
            set: function (value) {
                if(typeof value === "number" && value >= 5){
                    _price = value;
                }
            },
        });
        Object.defineProperty(this,"specialPrice",{
            set: function (value) {
                if(typeof value === "number" && value >= 0){
                    _specialPrice = value;
                }
            },
        });
        this.removeSpecialPrice = function(){
            _specialPrice = null;
        };
        //---------- Ende


    };

    Shoe.prototype = new Article();
    delete Article.prototype.price;

    return{
        Article: Article,
        Shoe : Shoe
    }
})();