let Interface = function(methods){
    this.methods = methods;
};
Interface.implements = function(object, ...interfaces){
    let counter = 0;
    let hasAllInterfaces = true;
    while (counter < interfaces.length){
        let interf = interfaces[counter];
        let methodsLen = interf.methods.length;
        let hasInterface = false;
        for (let i = 0 ; i < methodsLen ; i++){
            let method = interf.methods[i];
            if (object[method] && typeof object[method] == 'function'){
                hasInterface = true;
            }
        }
        if (!hasInterface) hasAllInterfaces = false;
        counter++;
    }
    return hasAllInterfaces;
};