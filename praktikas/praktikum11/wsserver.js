var WebSocketServer = require('websocket').server;
var http = require('http');
var fs = require('fs');

var formatDatePart = function(part) {
    let newPart = ("0"+(part));
    return newPart.substr(newPart.length-2, 2);
};



var sendTime = function(connection, message) {
  let actualDate = new Date();
  let outDate = actualDate.getFullYear()+"-"+
      formatDatePart(actualDate.getMonth()+1)+"-"+
      formatDatePart(actualDate.getDate())+" "+
      formatDatePart(actualDate.getHours())+":"+
      formatDatePart(actualDate.getMinutes())+":"+
      formatDatePart(actualDate.getSeconds())+"\n";
  connection.sendUTF(message.utf8Data+":"+outDate);
  setTimeout(()=>sendTime(connection,message),1000);
};

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);

    if (request.url==='/websocket') {
        response.writeHead(200, {'Content-Type': 'text/html'});
        fs.createReadStream("websocket.html").pipe(response);
    }
    if (request.url=='/socketWorker') {
        response.writeHead(200, {'Content-Type': 'text/html'});
        fs.createReadStream("socketWorker.html").pipe(response);
    }
    if (request.url=='/socketWorker.js') {
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        fs.createReadStream("socketWorker.js").pipe(response);
    }
    if (request.url=='/assets/cross.jpg') {
        response.writeHead(200, {'Content-Type': 'text/image/jpg'});
        fs.createReadStream("assets/cross.jpg").pipe(response);
    }

    if (request.url=='/serviceWorker.js') {
        response.writeHead(200, {'Content-Type': 'text/javascript'});
        fs.createReadStream("serviceWorker.js").pipe(response);
    }
});
server.listen(8088, function() {
    console.log((new Date()) + ' Websocket-Server is listening on port 8088');
});
function originIsAllowed(origin) {
  return true;
}

wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
wsServer.on('request', function(request) {
    var connection = request.accept('timer-protocol', request.origin);
    console.log((new Date()) + ' Connection accepted.');    
    
    connection.on('message', function(message) {
        console.log("Start timer");
        setTimeout(()=>sendTime(connection, message),1000);
    });
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});