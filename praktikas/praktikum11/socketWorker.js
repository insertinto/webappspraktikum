onconnect = function(e){
    var port = e.ports[0];
    let connection = new WebSocket('ws://localhost:8088/','timer-protocol');
    connection.onerror = function(error){
        console.log('WebSocket ' + error);
    };
    connection.onmessage = function(e){
        port.postMessage(e.data);
    };
    connection.onopen = function(){
        connection.send('Start timer:');
    }
};