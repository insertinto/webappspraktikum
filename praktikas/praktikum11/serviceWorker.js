var CACHE_NAME = 'site-cache';
var cacheURLs = ['/websocket.html','/wsserver.js','/assets/cross.jpg'];

this.addEventListener('install',function(event){
    event.waitUntil(
        caches.open(CACHE_NAME)
        .then(function(cache){  
            return cache.addAll(cacheURLs);
        })
    )
});

this.addEventListener('fetch',function(event){
    event.respondWith(
        caches.match(event.request)
        .then(function(response){
            if(response) {
                console.log(response);
                return response;
            }
            console.log("true");
            return fetch(event.request);
        }
        )
    ) 
});