// import {describe} from "mocha";
const mocha = require('mocha');
const describe = mocha.describe;
const expect = require("chai").expect;
let Article = require("../scripts/Article");

describe('ArticleConstructor', function() {
    describe('setName', function() {
        it('Name and price should be the same as set', function() {
            let article = new Article.Article("Cap");
            let articleName = article.name;
            expect(articleName).to.equal("Cap");
        });
    });
});