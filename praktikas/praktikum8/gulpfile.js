const gulp = require('gulp');
// var series = require('gulp');

const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');

const jsFiles = 'scripts/*.js';
const jsDest = 'dist';
const jsTestFiles = 'tests/*.js';


let concatAndSimplify = function() {
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
};

gulp.task('scripts', concatAndSimplify);



let checkCode = function(){
    return gulp.src(jsFiles)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
};

gulp.task('lint',checkCode);

let articleTest = function(){
    return gulp.src('tests/shopTest.js',{ read: false })
        .pipe(mocha({reporter: 'spec'}))
};

gulp.task('test',articleTest);


exports.default = function() {
    watch(jsFiles,concatAndSimplify);
};

