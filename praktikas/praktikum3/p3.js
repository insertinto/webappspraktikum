//------1------
let getRandomInt = function() {
    return Math.floor(Math.random() * 3);
};
//a
let addiere = (x,y) => x+y;
let subtrahiere = (x,y) => x-y;
let multipliziere = (x,y) => x*y;

//b
let rechne = function (operation1 , operation2, operation3) {
    let rand = getRandomInt();
    switch(rand){
        case 0: return operation1;
        case 1: return operation2;
        case 2: return operation3;
    }
};

console.log(rechne(addiere,subtrahiere,multipliziere)(1,3));
console.log(rechne(addiere,subtrahiere,multipliziere)(1,3));
console.log(rechne(addiere,subtrahiere,multipliziere)(1,3));

//c)
console.log("c:");
console.log(rechne(addiere(1,3),subtrahiere(1,3),multipliziere(1,3)));
/*Unterschied zu b):
Hier wird wird zwischen den zuvor berechneten ergebnissen ausgewählt, in b wird zuerst die funktion
zurückgegeben und dann berechnet.*/

//d
let rechne2 = function () {
    let addiere = (x,y) => x+y;
    let subtrahiere = (x,y) => x-y;
    let multipliziere = (x,y) => x*y;
    let rand = getRandomInt();
    switch(rand){
        case 0: return addiere;
        case 1: return subtrahiere;
        case 2: return multipliziere;
    }
};
console.log("d:");
console.log(rechne2()(1,3));
console.log(rechne2()(1,3));
console.log(rechne2()(1,3));

//------2------
//a
let addiereArguments = function () {
    let ret = 0;
    if (arguments.length===2) {
        for(let i of arguments){
            ret += i;
        }
        return ret;
    }
};

console.log("2 a:");
console.log(addiereArguments());
console.log(addiereArguments(1));
console.log(addiereArguments(1,2));
console.log(addiereArguments(1,2,3,4,5,6,7,8,9,10));

//b
let addiereArguments2 = function (x,y) {
    console.log(arguments.length);
    if (arguments.length===2) {
        return x+y;
    }else{
        return arguments[2](x,y);
    }
};

console.log("b:");
console.log(addiereArguments2(1,3,subtrahiere));
console.log(addiereArguments2(1,3));

//c
let anonymusAdd = (function(x,y){
    return x+y;
})(1,2);

console.log("c");
console.log(anonymusAdd);

//d
let rechne3 = function(x,y,operation) {
    var ergebnis = x-y;
    if (!operation) {
        (function(){
            var ergebnis = x + y;
            console.log(ergebnis);
        })();
    }
    return ergebnis;
};
console.log("c");
console.log(rechne3(1,3));
console.log(rechne3(1,3,addiere));

//------3------
//a
function operation(x,y){
    return x*y;
}

function berechne(x,y,z){
    let operation = function (x,y) {
        return x+y;
    };
    // return window.operation(operation(x,y),z);
    return this.operation(operation(x,y),z);
}
console.log("3a");
console.log(berechne(2,3,4));

//b
let math = {};
math.init = function(){
    math.addition=function(x,y){
        return x+y;
    };
    math.subtraktion = function(x,y){
        return x-y;
    }
};
console.log("b");
math.init();
console.log(math.addition(1,400));

//c
function ColorToggler(){
    let that = this;
    that.isRed = false;
    this.setRed = function(){
        console.log("FARBE ROT");
        this.style.backgroundColor = "red";
        that.isRed = true;
    };
    this.setBlue = function(){
        console.log("FARBE BLAU");
        this.style.backgroundColor = "blue";
        that.isRed = false;
    };
    /* Wie kann ich aus der handle funktion heraus setBlue() und setRed() aufrufen?
     * Wenn ich versuche die Funktion direkt aufzurufen, kann this.style nicht gefunden werden. Vermutlich weil
     * über this die referenz zum body fehlt.*/
    this.handle = function(){
        if(that.isRed){
            this.style.backgroundColor = "blue";
            that.isRed = false;
            // that.setBlue();
        }else{
            this.style.backgroundColor = "red";
            that.isRed = true;
            // that.setRed();
        }
    };
}
let body = document.getElementsByTagName("body")[0];
let colorController = new ColorToggler(body);

document.getElementById("setRed").addEventListener("click",colorController.setRed.bind(body));
document.getElementById("setBlue").addEventListener("click",colorController.setBlue.bind(body));
document.getElementById("switchColor").addEventListener("click",colorController.handle.bind(body));


