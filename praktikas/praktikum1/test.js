//Prakitkum 1
//----1----
/*
a)
- String.prototype.matchAll,
- import(),
- BigInt,
- Promise.allSettled,
- globalThis,
- for-in Mechanismen

Beschreibungen:
- globalThis represents the global object on any environment;
- Dynamic imports provide a way to conditionally import modules and use variables and interpolations to compose their paths;
- The import.meta object provides meta information about the current module. For now, the only available property is url;
- Now, it’s possible to define a name when on export * from statements;
- BigInt is a new data type which can hold numbers significantly bigger than the Number type;
- Promise.allSettled is similar to Promise.all, but will resolve even when some of the promises provided as argument is rejected;
- String.matchAll allows you to get all results matching a string against a regular expression;
- For-in loops now have a standardized order.

b)
-Im Juni jedes Jahres wird eine neue Version vorgestellt
*/

//----2----
//b)
let variable1 = Number.MAX_SAFE_INTEGER;
writeFat("2 B");
writeln("Max safe integer: "+ variable1); //9007199254740991
variable1 = variable1 * 10;
writeln("Aufgabe 2: "+ variable1);   //90071992547409900
//Die 1 am Ende fällt durch einen Rundungsfehler weg, da wir uns außerhalb des sicheren Integer-Bereiches befinden

//c)
writeFat("2 C");
variable1 = variable1 * variable1;
writeln(variable1);
variable1 = variable1 * variable1;
writeln(variable1);
variable1 = variable1 * variable1;
writeln(variable1);
variable1 = variable1 * variable1;
writeln(variable1);
variable1 = variable1 * variable1;
writeln(variable1);

//Der Wert ist infinity da wir den darstellbaren Wertebereich überschreiten.

//----3----

//a)
writeFat("3 A");
let ergebnis = Math.sqrt(-1);
writeln(ergebnis);
if (ergebnis) writeln("Ergebnis:"+ergebnis);
else writeln("Kein Ergebnis");
//Es gibt kein Ergebnis, da es keine Wurzel von negativen Werten gibt. $ergebnis ist somit NaN und somit falsly

//b)
writeFat("3 B");
writeln("2 == \'2\'"+compare1(2,'2'));
writeln("2 === \'2\'"+compare2(2,'2'));
function compare1(x,y){
    return (x == y);
}

function compare2(x,y){
    return (x === y);
}
//Bei  === wird der Typ der eingaben Verglichen, bei == nur die Wertigkeit, die bei unterschiedlichen Typen gecastet wird.


//----4----
writeFat("4");
var multiplikationVar = function(x,y) {
    var zahl = x;
    for (var counter = 0; counter<y; counter++) {
        var ergebnis = ergebnis?ergebnis + zahl:zahl;
    }
    return ergebnis;
};
writeln("Ergebnis:" + multiplikationVar(2,3));
/*Durch das Hoisting werden die function-scope Variablen zu Beginn deklariert. Deshalb wird der Wert der variable
* innerhalb der Schleife nur neu definiert. Somit kann der Wert auch außerhalb der Schleife*/


var multiplikationLet = function(x,y) {
    let zahl = x;
    let ergebnis;
    for (let counter = 0; counter<y; counter++) {
         ergebnis = ergebnis?ergebnis + zahl:zahl;
    }
    return ergebnis;
};
writeln("Ergebnis:" + multiplikationLet(2,3));
/*Bei der Verwendung von let greift durch den Block-Scope das Hoisting nicht und die Variable muss im Kopf
* der Funktion definiert werden. Würde die Variable außerhalb der Schleife deklairiert werden, kann darauf
* nicht zugegriffen werden.*/





